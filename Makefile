main: main.o init.o add.o print.o delete.o search.o release.o
	gcc -g -o main main.o init.o add.o print.o delete.o search.o release.o
main.o: main.c main.h
	gcc -c main.c
init.o: init.c
	gcc -c init.c
add.o: add.c
	gcc -c add.c
print.o: print.c
	gcc -c print.c
delete.o: delete.c
	gcc -c delete.c
search.o: search.c
	gcc -c search.c
release.o: release.c
	gcc -c release.c
clean: 
	rm main main.o init.o add.o print.o delete.o search.o release.o

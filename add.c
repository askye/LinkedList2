#include "main.h"

//This function will add a new node into the linked list and while assuring
//data is in order from smallest to largest (provided the node is not already
//in list).
//Program will print a message "SUCCESS", "NODE ALREADY IN LIST", or
//"OUT OF MEMORY".

int add(struct node*ll, int num)
{
struct node *new,*prev,*curr; //new node, prev and curr as temp holders
new=malloc(sizeof(struct node)); //allocate memory for new
prev=ll; //set prev temp to ll
curr=prev->next;
//if memory is not available
  if(new == NULL){
    printf("OUT OF SPACE\n");
    return(0); //do not add num 
  }
    new->data=num; //set data to specified num

int existN=search(ll,num); //check if node already in list

//node exists - insert node
  if(existN == 0){
    //if memory is available
    while(curr != NULL){
      if(num < curr->data){
        prev->next=new;
	new->next=curr; //loop to insert between prev and curr
	return(1); //success message
      }
      prev=curr;
      curr=curr->next;
    }
    //if num is inserted to end of list
    prev->next=new;
    new->next=NULL; //next point to NULL
    return (1); //success message
  }
  //if existN returns 1
  else{
    printf("NODE ALREADY IN LIST\n");
    return(0); //do not add num
  }
} //end of function

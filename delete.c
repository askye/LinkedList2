#include "main.h"

//This function will delete a node containing specified data. Program will
//print message "SUCCESS" if node is successfully deleted and
//"NODE NOT FOUND" if node does not exist.

int delete(struct node*ll, int num)
{
struct node *prev,*curr;
prev=ll; //sentinel
curr=prev->next; //where sentinel points
  while(curr != NULL){
    if(curr->data == num){
      prev->next=curr->next; //set previous to point to curr->next
      release(curr); //free node from memory 
      return(1); //success
    }
    prev=curr;
    curr=curr->next; //shift nodes 
  }
    return (0); //node not found
} //end of function

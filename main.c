#include "main.h"

// Amethyst Skye // CSE222 // 1-22-2021 //
// Programming Assignment 2 //
// A program written in C which allows the user to interact with a linked list
// with dynamic memory. 
// Command options for interacting with the list inclde:
// - i number - inserts a number into the list, placing it in order from 
// smallest to largest.
// - p - will print the list in order, on one line, separated by spaces.
// - s number - searches for the specified number and prints "FOUND" or
// "NOT FOUND".
// - d number - deletes the node containing the specified number and prints
// "SUCCESS" or "NODE NOT FOUND".
// - x - exits the program.
// Any other input - a synopsis of legal commands will be printed.
// After a command is exectures, the program will continue to prompt the user.

int main()
{
struct node *sent=init();//sentinel node
char str[120]; //string for user input
char input=0; //command interactions
int num=0; //number provided by user

//loop until user types 'x' to exit program//
while(input != 'x'){
  printf(">"); //user prompt symbol
  fgets(str,120,stdin);
  sscanf(str,"%c""%d",&input,&num); //recieves input command

//i - insert command
  if(input == 'i'){ 
    if((add(sent,num)) == 1){ //check return value from add
      printf("SUCCESS\n");
    }
  }
//p - print command
  else if(input == 'p'){
    print(sent); //use print function
  }
//s - search command
  else if(input == 's'){
    if((search(sent,num)) == 1){ //check return value of search
      printf("FOUND\n"); //node in list
    }
    else{
      printf("NOT FOUND\n"); //node not in list
    }
  }
//d - delete command
  else if(input == 'd'){
    if((delete(sent,num)) == 1){ //check return value from delete
      printf("SUCCESS\n"); //successful delete
    }
    else{
      printf("NODE NOT FOUND\n"); //node does not exist
    }
  }
//x - exit command
  else if(input == 'x'){
    release(sent); //release list from memory
    exit(0);
  }
//everything else
  else{
    printf("Please use one of the following commands:\n");
    printf("i number - inserts specified number to linked list\n");
    printf("p - prints current list\n");
    printf("s number - searches for specified number in list\n");
    printf("d number - deletes specified number\n");
    printf("x - exits program\n");
  }
} //end of while loop
} //end of main

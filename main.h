#include <stdio.h>
#include <stdlib.h>

//struct node definition
struct node{
	int data;
	struct node *next;
};

//function prototypes
struct node *init();
int add(struct node*,int);
void print(struct node*);
int delete(struct node*,int);
int search(struct node*,int);
void release(struct node*);

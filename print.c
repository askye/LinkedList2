#include "main.h"

//This function will iterate through the current linked list and print each
//number separated by a single space.

void print(struct node *ll)
{
struct node *curr;
curr=ll->next; //look at what sentinel is pointing to
while(curr != NULL){
  printf("%d ",curr->data);
  curr=curr->next;
} //end of while loop
printf("\n");
} //end of function

#include "main.h"

//This function will release all allocated memory

void release(struct node *ll)
{
struct node *temp,*release;
temp=ll; //set to sentinel node
  while(temp != NULL){
    release=temp;
    temp=temp->next; //release one node at a time
    free(release);
  }
} //end of function

#include "main.h"

//This functino will iterate through current linked list to find a specified
//piece of data. Program will print message "FOUND" if node exists, and 
//"NOT FOUND" if node does not exist.

int search(struct node *ll, int num)
{
struct node *curr;
curr=ll; 
  while(curr != NULL && curr->data != num){
    curr=curr->next; //loop through list
  }
  if(curr == NULL) return (0); //node does not exist
  return(1);
}//end of function
